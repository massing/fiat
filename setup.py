#!/usr/bin/env python

from distutils.core import setup

setup(name="FIAT", \
      version="1.1.0+", \
      description="FInite element Automatic Tabulator", \
      author="Robert C. Kirby", \
      author_email="robert.c.kirby@gmail.com", \
      url="http://www.math.ttu.edu/~kirby", \
      license="LGPL v3 or later", \
      packages=['FIAT'])
